Nessa pasta vamos recriar o que fizemos na [seção anterior](https://github.com/krzischp/mlserver_product_classifier), porém simplificando algumas coisas. 
- A única diferença aqui que vale a pena ressaltar é que estamos usando a imagem não-slim, pois o MLServer depende de algumas bibliotecas que só existem na versão completa da imagem python do docker hub. 
- Fora isso, o Dockerfile faz a instalação e configuração do nginx, a instalação dos pacotes python (incluindo MLServer), e a execução dos dois serviços por meio do script entrypoint.sh


Podemos testar o nosso modelo `iris_dtc_1.0.joblib`. Crie a imagem por meio do comando:
```bash
docker build -t mlserver .
```
E execute:
```bash
docker run -it -p 80:9361 -e PORT=9361 --rm --name mlserver-container mlserver
```


# GitLab

```bash
git init --initial-branch=main
git remote add origin https://gitlab.com/krzischp/mlserver.git
git add .
git commit -m "Initial commit"
git pull origin main --allow-unrelated-histories
git push -u origin main
```

**obs.**: nao esqueça de desativar a opçao "shared runners for this project", de cadastrar o seu runner e edirar o arquivo `~/.gitlab-runner/config.toml` assim:
```bash
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "minilinux"
  url = "https://gitlab.com/"
  token = "HpyD5Wzv6YPZgCrEKgGo"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
-    image = "python:3.9.6-slim"
+    image = "docker:19.03.12"
-    privileged = false
+    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
-    volumes = ["/cache"]
+    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```


# Notebooks

Iremos compartilhar esse notebook em um projeto no GitLab, por isso é importante esse cuidado. Assim, evite de instalar pacotes diretamente no notebook, preferindo deixar explícito no arquivo requirements.txt, assim outras pessoas poderão saber o que você está usando.

**Ambiente virtual**
```bash
conda deactivate

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv activate classificador-produtos
```

```bash
$HOME/.pyenv/versions/3.9.6/envs/classificador-produtos/bin/pip install notebook

$HOME/.pyenv/versions/3.9.6/envs/classificador-produtos/bin/jupyter notebook
```